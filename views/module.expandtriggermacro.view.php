<?php

require_once './include/config.inc.php';
require_once './include/hosts.inc.php';
require_once './include/maintenances.inc.php';
require_once './include/forms.inc.php';
require_once './include/users.inc.php';

require_once './include/page_header.php';
?>

<style>
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {background-color: #38383840;}
</style>
<header class="header-title"><nav class="sidebar-nav-toggle" role="navigation" aria-label="Sidebar control"><button type="button" id="sidebar-button-toggle" class="button-toggle" title="Show sidebar">Show sidebar</button></nav><div><h1 id="page-title-general">Expand Trigger Macro</h1></div></header>
<main>
<form method="post">
<div id="tabs" class="table-forms-container ui-tabs ui-widget ui-widget-content ui-corner-all" style="visibility: visible;">
<div id="maintenanceTab" aria-labelledby="tab_maintenanceTab" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-expanded="true" aria-hidden="false">
<ul class="table-forms" id="maintenanceFormList">
<li><div class="table-forms-td-left">
<label class="form-label-asterisk" for="host">Host </label>
</div>
<div class="table-forms-td-right">
 <input list="host" type="text" name="host" size="50" autocomplete="off">
		<datalist id="host">
<?php
$hosts = API::Host()->get(array(
                                'output' => array('host','name'),
                                ));
$arr_hosts=array();

foreach($hosts as $host){
	array_push($arr_hosts,$host['host']);
        if (!empty($host['name'])) array_push($arr_hosts,$host['name']);
}
$arr_hosts=array_unique($arr_hosts);
sort($arr_hosts);

foreach($arr_hosts as $name){
                ?><option value="<?php echo $name; ?>"><?php echo $name; ?></option><?php
        }
?></datalist>
<button type="submit" value="Search"/>Search Triggers</button>
<?php
if (isset($_POST['host'])) {
$triggers = API::Trigger()->get(array(
	'output' => array('triggerid','description','expression','priority'),
	'filter' => array('host' => $_POST['host'],'name' => $_POST['host']),
	'searchByAny' => 1,
	'expandExpression' => true,
	'expandDescription' => true,
	'sortfield' => "triggerid",
	'sortorder' => "ASC"
));

$severities= API::Settings()->get(array(
        'output' => array('severity_color_0','severity_color_1','severity_color_2','severity_color_3','severity_color_4','severity_color_5')
));
?><br/><p><div>
	Triggers with expanded macro found : <table>
	<tr>
		<th>Trigger name</th>
		<th>Trigger with expand expression</th>
	</tr>
	<?php 

	foreach($triggers as $trigger){
		echo "<tr>";
		echo "<td><a target='_blank' href='triggers.php?context=host&form=update&triggerid=" .$trigger['triggerid']. "'> ".$trigger['description'] ."</a></td>";
		echo "<td style='background-color: #".$severities["severity_color_$trigger[priority]"]."80'>". $trigger['expression']."</td>";
		echo "</tr>";
	}
}
?>
</table>
</div>
</p>
</main>

<?php

require_once './include/page_footer.php';
